//GET all
fetch('https://jsonplaceholder.typicode.com/todos').then(response => response.json()).then(todo => console.log(todo))


//GET single
fetch('https://jsonplaceholder.typicode.com/todos/1').then(response => response.json()).then(todo => console.log(`The item ${todo.title} on the list has a status of ${todo.completed}.`))


//MAP
//Answer from Sir Earl
/*
fetch('https://jsonplaceholder.typicode.com/todos').then(response => response.json()).then(todos => {
		let todo_list = todos.map(todo=> {
			//.map function loops trhough array and creates a new array which contains the data being returned.
		return todo.title 
			//Returns only the title property of each todo item
		})
	console.log(todo_list)
})
*/


//POST
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		id: 201,
		title: "Created To Do List Item",
		userId: 1
	})
}).then(response => response.json()).then(todo => console.log(todo))


//PUT
fetch('https://jsonplaceholder.typicode.com/todos/1', {

	method: 'PUT',
	headers:{
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structure",
		id: 1,
		status: "Pending",
		title: 'Updated To Do List Item',
		userId: 1
	})
}).then(response => response.json()).then(todo => console.log(todo))


//PATCH
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PATCH',
	headers:{
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		dateCompleted: "07/09/21",
		id: 1,
		status: "Complete",
		title: 'delectus aut autem',
		userId: 1
	})
}).then(response => response.json()).then(todo => console.log(todo))


//DELETE
fetch('https://jsonplaceholder.typicode.com/posts/2', {
	method: 'DELETE'
}).then(response => response.json()).then(todo => console.log(todo))
